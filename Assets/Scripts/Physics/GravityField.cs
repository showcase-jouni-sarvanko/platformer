﻿using UnityEngine;

public class GravityField : MonoBehaviour
{
	public enum FieldType {Point, Direction};
	public FieldType fieldType = FieldType.Point;

	public float forceMagnitude = 1f;
	public Vector2 forceDirection = new Vector2(0f, -1f); //Vector2.zero;
	public float distanceScale = 1f;
	
	public Vector2 GetGravityStrength(Vector3 targetPosition)
	{
		switch(fieldType)
		{
			case FieldType.Point:
				Vector2 forceVector = transform.position - targetPosition;
				Vector2 direction = forceVector.normalized;

				return direction * forceMagnitude;

			case FieldType.Direction:
			
				return forceDirection * forceMagnitude;
		}
		
		return Vector2.zero;
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		ObjectController controller = collider.gameObject.GetComponentInParent<ObjectController>();
		
		if(controller != null)
		{
			if(!controller.gravityFields.Contains(this))
			{
				controller.gravityFields.Add(this);
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D collider)
	{
		ObjectController controller = collider.gameObject.GetComponentInParent<ObjectController>();
		
		if(controller != null)
		{
			if(controller.gravityFields.Contains(this))
			{
				controller.gravityFields.Remove(this);
			}
		}
	}	
}
