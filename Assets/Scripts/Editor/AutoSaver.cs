﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class AutoSaver
{
	static double autoSaveInterval = 300f;
	static double previousSaveTime = 0f;

	static AutoSaver()
	{
		Debug.Log("AutoSaver initialized");
		EditorApplication.playModeStateChanged += CheckPlaymodeStateChange;
		EditorApplication.update += CheckAutoSave;
	}

	static void CheckAutoSave()
	{
		if(!Application.isPlaying)
		{
			if(EditorApplication.timeSinceStartup > previousSaveTime + autoSaveInterval)
			{
				Debug.Log("Timed auto save");

				Scene currentScene = EditorSceneManager.GetActiveScene();

				SaveScene(currentScene);

				previousSaveTime = EditorApplication.timeSinceStartup;
			}
		}
	}

	static void CheckPlaymodeStateChange(PlayModeStateChange state)
	{
		if(!EditorApplication.isPlayingOrWillChangePlaymode || EditorApplication.isPlaying) 
			return;

		if(state == PlayModeStateChange.ExitingEditMode)
		{
			Debug.Log("Saving scene before entering Play mode.");

			Scene currentScene = EditorSceneManager.GetActiveScene();

			SaveScene(currentScene);
		}
	}

	static void SaveScene(Scene scene)
	{
		SaveScene(scene, scene.path);
	}

	static void SaveScene(Scene scene, string path)
	{
		bool succesful = EditorSceneManager.SaveScene(scene, path);

		Debug.Log("Saving scene "+ scene.name +" : "+ (succesful ? "OK" : "Error!"));

		AssetDatabase.SaveAssets();
	}
}