﻿using UnityEngine;

public class WalkAudioManager : MonoBehaviour
{
    public AudioClip[] clips;

    AudioSource source;

    int index = -1;
    int previousIndex = -1;

    public float minPitch;
    public float maxPitch;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void Play()
    {
        if (!source.isPlaying)
        {
            float pitch = Random.Range(minPitch, maxPitch);
            source.pitch = pitch;

            if (clips.Length > 1)
            {
                while (index != previousIndex)
                {
                    index = Random.Range(0, clips.Length);
                }
            }
            else
            {
                index = 0;
            }

            previousIndex = index;

            AudioClip walkClip = clips[index];

            source.PlayOneShot(walkClip);
        }
    }
}
