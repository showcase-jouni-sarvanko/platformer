﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D collider)
	{	
		if(collider.CompareTag("Player"))
		{
			ObjectController controller = GetComponent<ObjectController>();
			
			if(controller != null)
			{
				controller.Die();
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.collider.CompareTag("Player"))
		{
			ObjectController controller = collision.gameObject.GetComponent<ObjectController>();
			
			if(controller != null)
			{
				controller.Die();
			}
		}
	}
}
