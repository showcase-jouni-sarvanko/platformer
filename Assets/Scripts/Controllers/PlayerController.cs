﻿using UnityEngine;

public class PlayerController : ObjectController
{
	Animator animator;
	Rigidbody2D rbody;
	
	public float jumpSpeed = 5f;
	public float deathJumpSpeed = 1f;
	
	public float walkingSpeed = 5f;
	public float climbingSpeed = 5f;

	private float horizontalInput;
	private float verticalInput;
	private bool jumped;
	
	public Vector2 inputMovement = Vector2.zero;

	public AudioSource walkAudio;
	public AudioSource jumpAudio;
	
	public Transform visuals;

	void Start ()
	{
		animator = GetComponentInChildren<Animator>();
		rbody = GetComponent<Rigidbody2D>();
		
		ChangeState(CharacterState.OnGround);
	}

    protected override void CheckInput()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        jumped = Input.GetKeyDown(KeyCode.Space);
    }

    protected override void UpdateMovement()
    {
        horizontalSpeed = 0f;
        verticalSpeed = 0f;

        switch (characterState)
		{
			case CharacterState.OnGround:

				gravityVelocity = currentGravityForce.normalized;

				TurnCharacter(surfaceNormal);

				if(jumped)
				{
					jumpAudio.Play();
					ChangeState(CharacterState.InAir);
					gravityVelocity += (Vector2)transform.up * jumpSpeed;
				}
			
				if(horizontalInput != 0f)
				{
					walkAudio.volume = Mathf.Abs( horizontalInput );
				
					if(!walkAudio.isPlaying)
					{
						walkAudio.Play();
					}
				}
				else
				{
					walkAudio.Stop();
				}
				
				horizontalSpeed = horizontalInput * walkingSpeed;

				transform.localScale = new Vector3(Mathf.Sign(horizontalSpeed), 1f, 1f);
				animator.SetFloat("horizontalSpeed", Mathf.Abs(horizontalInput));

				if(verticalInput < 0f)
				{
					ChangeState(CharacterState.InAir);
					IgnorePlatforms(true);
				}

				break;

			case CharacterState.InAir:

                if (currentGravityForce.magnitude > 0f)
                {
                    gravityVelocity += currentGravityForce * Time.deltaTime;

                    dropSpeed = -Vector2.Dot(gravityVelocity, currentGravityForce);

                    TurnCharacter(-currentGravityForce.normalized);
                }
                else
                {
                    dropSpeed = 0;
                }
			
				walkAudio.Stop();
			
				horizontalSpeed = horizontalInput * walkingSpeed;
				animator.SetFloat("horizontalSpeed", Mathf.Abs(horizontalInput));
				
				animator.SetFloat("verticalSpeed", dropSpeed);

				break;

			case CharacterState.Climbing:

				gravityVelocity = Vector2.zero;

				TurnCharacter(climbDirection);

				walkAudio.Stop();

				horizontalSpeed = horizontalInput * climbingSpeed;
				verticalSpeed = verticalInput * climbingSpeed;

				animator.SetFloat("verticalSpeed",  verticalInput);
				
				break;

			case CharacterState.Dead:
				gravityVelocity += currentGravityForce * Time.deltaTime;
				
				visuals.Rotate(0f, 0f, 90f * Time.deltaTime);

				animator.SetFloat("verticalSpeed",  gravityVelocity.y);
				break;

			default:
				break;
		}
		
		inputMovement = 
			(Vector2)transform.right * horizontalSpeed + 
			(Vector2)transform.up * verticalSpeed;
		
		rbody.velocity = gravityVelocity + inputMovement;
	}

    public override void ChangeState(CharacterState nextState)
	{
		if(nextState == characterState)
			return;
	
		switch(nextState)
		{
		    case CharacterState.OnGround:
			    IgnorePlatforms(false);

			    animator.SetBool("climbing", false);
			    animator.SetBool("inAir", false);
			
			    break;

		    case CharacterState.InAir:
			    IgnorePlatforms(false);

			    animator.SetBool("climbing", false);
			    animator.SetBool("inAir", true);
			
			    break;

		    case CharacterState.Climbing:
			    IgnorePlatforms(true);

			    animator.SetBool("climbing", true);
			    animator.SetBool("inAir", false);
			
			    break;
			
		    case CharacterState.Dead:
			    animator.SetBool("climbing", false);
			    animator.SetBool("inAir", true);
			    break;

			default:
			    break;
		}
		
		characterState = nextState;
	}
	
	void IgnorePlatforms(bool isIgnored)
	{
		Physics2D.IgnoreLayerCollision(
			gameObject.layer, 
			LayerMask.NameToLayer("Platform"), 
			isIgnored);
			
		// Reset the player layer
		// Lets the engine to know that it should not collide with a collider which is touching it
		gameObject.layer = gameObject.layer;
	}

	protected override void CheckClimbing()
	{
		if(verticalInput != 0f && canClimb)
		{
			ChangeState(CharacterState.Climbing);
		}
	}

	public override void Die()
	{
		Collider2D [] colliders = GetComponents<Collider2D>();
		
		foreach(Collider2D collider in colliders)
		{
			collider.enabled = false;
		}
		
		ChangeState(CharacterState.Dead);
		
		gravityVelocity = (transform.up + transform.right).normalized * deathJumpSpeed;
	}
}
