﻿using UnityEngine;

public class CameraMovementController : MonoBehaviour
{
	public Transform target;
    public float followRatio = 1f;
    Vector3 previousPosition;

    public bool canRotate = true;

    public bool keepWithinBounds = false;
    public float bounds = 0f;

    void Start()
    {
        if(target)
        {
            previousPosition = target.position;

            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
        }
    }

	#if UNITY_EDITOR
    void OnValidate()
    {
        if (target)
        {
            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
        }
    }
	#endif

    void LateUpdate ()
	{
        if (target)
        {
            Vector3 targetMovement = target.position - previousPosition;
            Vector3 newPosition = transform.position + targetMovement * followRatio;

            if (keepWithinBounds)
            {
                Vector2 distanceVector = newPosition - target.position;

                if(Mathf.Abs(distanceVector.x) > bounds)
                {
                    newPosition.x -= Mathf.Sign(distanceVector.x) * bounds;
                }

                if (Mathf.Abs(distanceVector.y) > bounds)
                {
                    newPosition.y -= Mathf.Sign(distanceVector.y) * bounds;
                }
            }

            transform.position = newPosition;

            previousPosition = target.position;

            if(canRotate)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, 0.01f);
            }
        }
	}
}
