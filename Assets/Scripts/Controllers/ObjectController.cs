﻿using UnityEngine;
using System.Collections.Generic;

public abstract class ObjectController : MonoBehaviour
{
	public enum CharacterState {OnGround, InAir, Climbing, Dead};
	[HideInInspector] public CharacterState characterState;

	[HideInInspector] public bool canClimb;
	[HideInInspector] public Vector2 climbDirection;

	[HideInInspector] public float horizontalSpeed;
	[HideInInspector] public float verticalSpeed;

	[HideInInspector] public List<GravityField> gravityFields = new List<GravityField>();
	[HideInInspector] public Vector2 currentGravityForce;

	[HideInInspector] public Vector2 surfaceNormal = Vector2.up;
	[HideInInspector] public GravityField surfaceGravityField = null;

    public CircleCollider2D feetCollider;

    public Transform groundCheck;
    public LayerMask groundCheckMask;

    Vector2 groundCheckStartPosition;
	Vector2 groundCheckDirection;
    float groundCheckRadius;
    float groundCheckDistance;

	[HideInInspector] public Vector2 gravityVelocity = Vector2.zero;
	[HideInInspector] public float dropSpeed = 0f;
	[HideInInspector] public float velocityGravityAngle;

    public Transform head;
    public Transform feet;

    public bool onGround = false;

    public abstract void Die();
    public abstract void ChangeState(CharacterState nextState);

	protected abstract void CheckInput();
    protected abstract void UpdateMovement();
    protected abstract void CheckClimbing();

    protected void Update()
	{
        CheckGravity();
        CheckInput();
        CheckState();
        UpdateMovement();

        onGround = false;
    }

    void CheckGravity()
    {
        if (surfaceGravityField)
        {
            currentGravityForce = surfaceGravityField.GetGravityStrength(transform.position);
        }
        else
        {
            currentGravityForce = Vector2.zero;

            foreach (GravityField gravityField in gravityFields)
            {
                currentGravityForce += gravityField.GetGravityStrength(transform.position);
            }
        }
    }

    protected void CheckState()
    {
        switch (characterState)
        {
            case CharacterState.OnGround:
                GroundCheck();

                CheckClimbing();

                break;
            case CharacterState.InAir:

                velocityGravityAngle = Vector2.Angle(gravityVelocity, currentGravityForce);

                if (90f > velocityGravityAngle && !InsidePlatform())
                    GroundCheck();

                CheckClimbing();

                break;
            case CharacterState.Climbing:

                if (!canClimb)
                {
                    GroundCheck();
                }

                break;

            default:
                break;
        }
    }


    protected void GroundCheck()
    {
        groundCheckStartPosition = transform.position + transform.up * feetCollider.offset.y;
        groundCheckRadius = feetCollider.radius * 0.9f;
        groundCheckDirection = -transform.up;
        groundCheckDistance = Mathf.Abs(groundCheck.localPosition.y - feetCollider.offset.y) - groundCheckRadius;

        RaycastHit2D hit = Physics2D.CircleCast(
                                groundCheckStartPosition,
                                groundCheckRadius,
                                groundCheckDirection,
                                groundCheckDistance,
                                groundCheckMask.value);

        if (hit.collider != null)
        {
            surfaceNormal = hit.normal;

            surfaceGravityField = hit.collider.gameObject.GetComponentInParent<GravityField>();

            ChangeState(CharacterState.OnGround);
        }
        else
        {
            surfaceGravityField = null;

            ChangeState(CharacterState.InAir);
        }
    }

    public void EnteredLadder(Vector2 ladderDirection)
    {
        canClimb = true;
        climbDirection = Mathf.Sign(Vector2.Dot(transform.up, ladderDirection)) * ladderDirection;
    }

    public void ExitedLadder()
    {
        canClimb = false;
    }

    bool InsidePlatform()
    {
        RaycastHit2D hit = Physics2D.Linecast(
                        head.position,
                        feet.position,
                        LayerMask.GetMask(new string[] { "Platform" }));

        if (hit.collider != null)
            return true;
        else
            return false;
    }

    protected void TurnCharacter(Vector2 direction)
    {
        transform.up = direction;
        transform.eulerAngles = new Vector3(0f, 0f, transform.eulerAngles.z);
    }

	#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(groundCheckStartPosition, groundCheckRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCheckStartPosition + groundCheckDirection * groundCheckDistance, groundCheckRadius);
    }
	#endif
}
