﻿using UnityEngine;

public class AIController : ObjectController
{
	public float speed = 5f;
    public float direction = 1f;

	Rigidbody2D rbody;

	void Start ()
	{
		rbody = GetComponent<Rigidbody2D>();

		rbody.velocity = transform.right * speed;
		
		characterState = CharacterState.OnGround;
	}

    protected override void CheckInput()
    {

    }

    protected override void UpdateMovement()
    {
		switch(characterState)
		{
			case CharacterState.OnGround:
                gravityVelocity = currentGravityForce.normalized;

                TurnCharacter(surfaceNormal);

                horizontalSpeed = direction * speed;

                transform.localScale = new Vector3(Mathf.Sign(horizontalSpeed), 1f, 1f);

                break;
            case CharacterState.InAir:

                if (currentGravityForce.magnitude > 0f)
                {
                    gravityVelocity += currentGravityForce * Time.deltaTime;

                    TurnCharacter(-currentGravityForce.normalized);
                }

                break;
            case CharacterState.Dead:
			
				float yScale = transform.localScale.y; 
			
				transform.localScale = new Vector3(
											Mathf.Sign(rbody.velocity.x),
			           						Mathf.Lerp(yScale, 0f, 0.1f),
			           						1f);
				
				break;
		}

        rbody.velocity =
            gravityVelocity + 
            (Vector2)transform.right * horizontalSpeed +
            (Vector2)transform.up * verticalSpeed;
    }

    protected override void CheckClimbing()
    {

    }

    public override void ChangeState(CharacterState nextState)
    {
        if (nextState == characterState)
            return;

        switch (nextState)
        {
            case CharacterState.OnGround:

                break;
            case CharacterState.InAir:

                break;
            case CharacterState.Climbing:

                break;

            case CharacterState.Dead:

                break;
            default:
                break;
        }

        characterState = nextState;
    }

    public override void Die()
	{
		Collider2D [] colliders = GetComponentsInChildren<Collider2D>();
		
		foreach(Collider2D collider in colliders)
		{
			collider.enabled = false;
		}
		
		characterState = CharacterState.Dead;
	}
}
