﻿using UnityEngine;

public class Ladder : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D collider)
	{
		ObjectController controller = collider.gameObject.GetComponentInParent<ObjectController>();
		
		if(controller != null)
		{
            controller.EnteredLadder(transform.up);
		}
	}
	
	void OnTriggerExit2D(Collider2D collider)
	{
		ObjectController controller = collider.gameObject.GetComponentInParent<ObjectController>();
		
		if(controller != null)
        {
            controller.ExitedLadder();
        }
    }
}